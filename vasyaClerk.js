// https://www.codewars.com/kata/vasya-clerk/javascript

function tickets(peopleInLine){
  const cashBox = {
    25: 0,
    50: 0,
  };
  const result = peopleInLine.every(manMoney => {
    switch(manMoney) {
      case 25: {
        cashBox[25] += 1;
        return true;
      }
      case 50: {
        if (cashBox[25]) {
          cashBox[25] -= 1;
          cashBox[50] += 1;
          return true
        }
        return false;
      }
      case 100: {
        if (cashBox[25] && cashBox[50]) {
          cashBox[25] -= 1;
          cashBox[50] -= 1;
          return true;
        } else if (cashBox[25] >= 3) {
          cashBox[25] -= 3;
          return true;
        }
        return false;
      }
    }
  })
  return result ? 'YES' : 'NO';
}