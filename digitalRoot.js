// https://www.codewars.com/kata/sum-of-digits-slash-digital-root
function digital_root(n) {
  if (n.toString().length === 1) {
    return n;
  }
  const result = n.toString().split('').reduce((sum, digit) => sum += +digit, 0);
  return digital_root(result);
}