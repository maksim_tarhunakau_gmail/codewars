// https://www.codewars.com/kata/duplicate-encoder/javascript

function duplicateEncode(word){
  const obj = {};
  const lettersArr = word.toLowerCase().split('');
  lettersArr.forEach(letter => {
    if (typeof obj[letter] === 'undefined') {
      obj[letter] = 1;
    } else {
      obj[letter] += 1;
    }
  })
  return lettersArr.map(letter => obj[letter] === 1 ? '(' : ')').join('');
}
