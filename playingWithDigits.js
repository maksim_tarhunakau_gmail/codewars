// https://www.codewars.com/kata/playing-with-digits/javascript

function digPow(n, p){
  const resultSum = n.toString().split('').reduce((sum, digit) => {
    sum += Math.pow(digit, p);
    p++;
    return sum;
  }, 0)          
  return resultSum % n === 0 ? resultSum / n : -1;
}