// https://www.codewars.com/kata/your-order-please/javascript

function order(words){
  const wordsArr = words.split(' ');
  return wordsArr.reduce((resultArr, word) => {
    for(let i = 0; i < wordsArr.length; i++) {
      if (word.includes(i+1)) {
        resultArr[i] = word;
        break;
      }
    }
    return resultArr;
  }, [])
  .join(' ');
}