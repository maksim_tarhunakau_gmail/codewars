https://www.codewars.com/kata/sequence-of-power-digits-sum/train/javascript


function sumPowDigSeq(currentValue, degree, k) {
  let i = 0;
  let resultArr = [];
  let h = -1;
  let lastValue = -1;
  while(i !== k) {
    if(h === -1) { // if was not found cycle pattern
      resultArr.push(currentValue);
    }
    currentValue = currentValue.toString().split('').reduce((sum, digit) => sum += Math.pow(digit, degree), 0);
    if (resultArr.includes(currentValue) && h === -1) { // if values begining to repeat
      let startCycleIndex = resultArr.indexOf(currentValue);
      h = startCycleIndex;
      resultArr = resultArr.slice(startCycleIndex, i + 1);
    }
    if (i + 1 === k) { // get last value
      lastValue = currentValue;
    }
    i++;
  }
  return [h, resultArr, resultArr.length, lastValue];
}


//new variant
function sumPowDigSeq(currentValue, degree, k) {
  let resultArr = [currentValue];
  let startIndex = -1;
  let endIndex = -1;
  for(let i = 0; i < k; i++) {
    currentValue = currentValue.toString().split('').reduce((sum, digit) => sum += Math.pow(digit, degree), 0);
    if (resultArr.includes(currentValue) && startIndex === -1) { // if values begining to repeat
      startIndex = resultArr.indexOf(currentValue);
      endIndex = i + 1;
    }
    resultArr.push(currentValue);
  }
  return [startIndex, resultArr.slice(startIndex, endIndex), resultArr.slice(startIndex, endIndex).length, resultArr.pop()];
}