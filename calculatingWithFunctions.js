// https://www.codewars.com/kata/calculating-with-functions/javascript
function zero(func) {
  return !func ? 0 : func(0);  
}
function one(func) {
  return !func ? 1 : func(1);  
}
function two(func) {
  return !func ? 2 : func(2);  
}
function three(func) {
  return !func ? 3 : func(3);  
}
function four(func) {
  return !func ? 4 : func(4);  
}
function five(func) {
  return !func ? 5 : func(5);  
}
function six(func) {
  return !func ? 6 : func(6);    
}
function seven(func) { 
  return !func ? 7 : func(7);  
}
function eight(func) {
  return !func ? 8 : func(8);  
}
function nine(func) {
  return !func ? 9 : func(9);  
}

function plus(secondDigit) {
  return (firstDigit) => Math.floor(firstDigit + secondDigit);
}
function minus(secondDigit) {
  return (firstDigit) => Math.floor(firstDigit - secondDigit);
}
function times(secondDigit) {
  return (firstDigit) => Math.floor(firstDigit * secondDigit);
}
function dividedBy(secondDigit) {
  return (firstDigit) => Math.floor(firstDigit / secondDigit);
}
