// https://www.codewars.com/kata/equal-sides-of-an-array
function findEvenIndex(arr)
{
  let searchIndex = -1;
  let leftSideSum = 0;
  for( let i = 0; i < arr.length; i++) {
    const rightSideSum = arr.slice(i + 1).reduce((sum, elem) => sum += elem, 0); //gets sum of right part of array
    if (leftSideSum === rightSideSum) {
      searchIndex = i;
      break;
    }
    leftSideSum += arr[i];
  }
  return searchIndex;
}