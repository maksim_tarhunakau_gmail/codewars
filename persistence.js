// https://www.codewars.com/kata/persistent-bugger
function persistence(num) {
  let counter = 0;
  while(num.toString().length !== 1) {
    num = num.toString().split('').reduce((result, digit) => result*digit, 1);
    counter++;
  }
  return counter;
}