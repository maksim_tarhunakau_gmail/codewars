
// https://www.codewars.com/kata/find-the-odd-int/javascript
function findOdd(A) {
  const obj = A.reduce((obj, elem) => {
    if (typeof obj[elem] === 'undefined') {
      obj[elem] = 1;
    } else {
      obj[elem] += 1;  
    }
    return obj;
  }, {})
  // gets all entries, find element which value is odd and get element name converted to number
  return +Object.entries(obj).find((element) => element[1] % 2 !== 0)[0]; 
}

//new variant
const obj = {};
for (let i = 0; i < A.length; i++) {
  const element = A[i];
  if (typeof obj[element] === 'undefined') {
     obj[element] = 1;
  } else {
    obj[element] += 1;
  }
}
const stringNumber = Object.entries(obj).find((element) => element[1] % 2 !== 0)[0];
return Number.parseInt(stringNumber);