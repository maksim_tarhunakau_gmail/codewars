// https://www.codewars.com/kata/iq-test

function iqTest(numbers){
  const numbersArr = numbers.split(' ').map(number => +number);
  const even = numbersArr.filter(a => a % 2 === 0);
  const odd = numbersArr.filter(a => a % 2 !== 0);
  return even.length === 1? numbersArr.indexOf(even[0])+1 : numbersArr.indexOf(odd[0])+1;
}