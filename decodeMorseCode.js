// http://www.codewars.com/kata/decode-the-morse-code/javascript

decodeMorse = function(morseCode) {
  wordsArr = morseCode.trim().split(' ');
  let decodedString = '';
  for (let i = 0; i < wordsArr.length; i++) {
    if (wordsArr[i] === '') { //case with space
      decodedString += ' ';
      i++; //just skip next element
    } else {
      decodedString += MORSE_CODE[wordsArr[i]];
    }
  }
  return decodedString;
}

//NEW VERSION
decodeMorse = function(morseCode) {
  return morseCode.trim().split('   ')
                  .map((word) => { 
                    return word
                              .split(' ')
                              .map((letter) => MORSE_CODE[letter])
                              .join('')
                  })
                  .join(' ');
}