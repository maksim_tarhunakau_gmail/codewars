//http://www.codewars.com/kata/514b92a657cdc65150000006/

function solution(number){
  if (number < 3) {
   return 0;
  }
  let sum = 0;
  for(let i = 3, j = 5; i < number; i += 3) {
    if (j < number) {
      if (j % 3 !== 0) {
        sum += j;  
      }
      j += 5;
    }
    sum += i;
  }
  return sum;
}