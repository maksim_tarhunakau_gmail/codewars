// https://www.codewars.com/kata/tribonacci-sequence/train/javascript

function tribonacci(signature,n){
  if (n <= 3) {
    return signature.slice(0, n);
  } else {
    for(let i = 4; i <= n; i++) {
      signature.push(signature.slice(i-4, i).reduce((sum, element) => sum += element));
    }
    return signature;
  }
}